import sqlite3 from './connector-sqlite3'
import * as Hps from './helpers'
// Debug message setup
const debug = Hps.debugGen('1Goal.dbMaint')

export default class dbMaint{
  public db:sqlite3
  public sqlStrings = {
    init: [ "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, nickname TEXT default('用户'), realname TEXT, openid TEXT UNIQUE NOT NULL, role text default('0'), mobile INTEGER, lastMsg TEXT, focus TEXT,headimgurl TEXT, subscribe INTEGER, verified INTEGER default(0), currentGoal REFERENCES goals(id), comment TEXT, lastActive TEXT DEFAULT(DATETIME('now')), created TEXT DEFAULT(DATETIME('now')), modified TEXT DEFAULT(DATETIME('now')))",
            "CREATE INDEX i1 on users(openid)",
            "CREATE TRIGGER IF NOT EXISTS update_time_users after UPDATE ON users FOR EACH ROW BEGIN UPDATE users SET modified = DATETIME('now') WHERE rowid = OLD.rowid; END;",
            "CREATE TABLE IF NOT EXISTS goals (id INTEGER PRIMARY KEY AUTOINCREMENT, userid REFERENCES users(openid) NOT NULL, content TEXT, status INTEGER, comment TEXT, due TEXT DEFAULT(DATETIME('now','+1 day')), created TEXT DEFAULT(DATETIME('now')), modified TEXT DEFAULT(DATETIME('now')))",
            "CREATE TRIGGER IF NOT EXISTS update_time_goals after UPDATE ON goals FOR EACH ROW BEGIN UPDATE goals SET modified = DATETIME('now') WHERE rowid = OLD.rowid; END;",
            "CREATE TABLE IF NOT EXISTS requests (id INTEGER PRIMARY KEY AUTOINCREMENT, fromUser TEXT NOT NULL, toSever TEXT, type TEXT, content text, comment TEXT, created TEXT DEFAULT(DATETIME('now')), modified TEXT DEFAULT(DATETIME('now')))",
            "CREATE TRIGGER IF NOT EXISTS update_time_requests after UPDATE ON requests FOR EACH ROW BEGIN UPDATE requests SET modified = DATETIME('now') WHERE rowid = OLD.rowid; END;",
            "PRAGMA foreign_keys = ON"
    ]
  }

  
  constructor(dbRef:sqlite3){
    this.db = dbRef
  }

  public init(){
    return Promise.resolve().then(()=>{
      if (!this.db.sync) {
        return this.db.init()
      }
    }).then(()=>{
      var p = Promise.resolve()
      for (let sqlString of this.sqlStrings.init){
         p = p.then(()=>{
           debug("Running DB structure init script: ", sqlString)
           return this.db.pool.run(sqlString)
         })
       }
      return p
    }).catch((e)=>{
    debug("Something wrong with DB structure init!!!", e)
  })
  }
}



