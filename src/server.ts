// Enable live cmd edit [experimental]
const repl = require("repl").start({useGlobal:true, prompt:"$> "})

// port def(3001 for formal release, 4000 for test)
let port = 4001

import app from "./wechatLib"
import * as Hps from "./helpers"
// var Promise = require('bluebird')

(<any>global).app = app
global['app'].init().then(()=>{
  console.log(`starting listening on ${port}`)
  global['app'].listen(port)
})



