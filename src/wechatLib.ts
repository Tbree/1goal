// Wechat module
Promise = require('bluebird')
import * as blue from 'bluebird'
import * as https from 'https'
import * as crypto from 'crypto'
import * as express from 'express'
import * as parser from 'body-parser'
import * as helmet from 'helmet'
import * as Hps from './helpers'
import * as moment from 'moment'
moment.locale('zh-cn')
import Models from './models'
// import * as ns from 'node-schedule'

// Debug message setup
const debug = Hps.debugGen('TInfo.wechatLib')

// Wchat APP setting
// account info of formal account 
// const we_token = 'scaltcft'
// const we_secret = 'd46e5ec4cb3acddb07b90022064e13b9'
// const we_id = 'wxcca42bacdbeaf7db'
// const we_AESKey = 'Zym6Aj9H75icOLUjolftN180cjav7QjGCGqanYpvJKF'

// account info of tset account
const we_token = 'wangou'
const we_secret = '73d5bbbde6804a0766b17cccf70f67fb'
const we_id = 'wx97b2219c6b2a08d3'
const we_AESKey = ''
let we_accToken = {
  token: "",
  expireAt: 0
}



// Maint component app

interface app extends express{
  use: express.use
  models: Models  // Models from database. models.db is a reference of DB in use.
  bypassWechatAuth:boolean  // Bypass flag if tested by mock wechat server
  init  //initialization
  sendMsg // helper to send message to user
  wxMsgKeys // Structure definition of keys in wechat server message
  findid  // helper to find user id using openid
  getUserInfo //helper to get user info from wechat server
  updateMenu // helper to update wechat menu
  msgGen //helper to generate suitable message ( type, language, etc. )
  regs //RegExp for text pattern matching. 
  getToken //
  queryDB //
  options:{
    authRequired:boolean      // Authentication before query
    bypassWechatAuth:boolean  // Bypass flag if tested by mock wechat server
    enableBlasklist:boolean   // Blacklist certain instructors
    blasklist:Array<string>
    admins:Array<string>
  }
  dataServerAvail:Boolean
}

const app:app = express()
app.use(helmet())
app.use(logger)
app.use(wechatAuth)
app.use(parser.text({type:'*/*'}))
app.use(dispatcher)
app.use(finalizer)
app.use(errHdlr)

// test flag
app.bypassWechatAuth = false
app.options.bypassWechatAuth = false
app.options.authRequired = true
app.options.enableBlasklist = false
app.dataServerAvail = true
app.options.blasklist = [
  "曹俊","李霞","孙懿","王文涛","王一帆","谢俊","朱林松","党锋","寇红","李世君","梁华兴","刘斌D","陆大卫","马振华","潘友彬","荣志远","孙业荣","王真","谢铁民","徐崇明","杨俊龙","于卫国"
]
app.options.admins = [
  "ois-5s7zctSaAgzG0ROk3S0epALE",
  "olYu60fb5fW8AqduhPriTbTAUBsw"
]

// RegExp patterns
app.regs = {
  days: /^\d{1,3}$/,
  date: /^201\d{5}/,
  verify: /^([\u4e00-\u9fa5A-Za-z]{2,6})[\s+.。;'-_=](\d{11})$/,
  name: "[^\u4e00-\u9fa5]"
}

// DB initialization
let dbOpt = {db:'sqlite3', path:'../db/database.sqlite3'}
const models = new Models(dbOpt)
app.models = models

app.init = async function () {
  return models.init().then(async ()=>{
      debug('DB init complete')
      app.getToken()
    }).catch((e)=>{
      debug('Error occured during DB init')
    })
}





export default app

// Global vars
// 1. Incoming requests counter
var incomingCnt = 1
// 2. Messageid set for duplication check
var msgIds = new Set()




// get token
app.getToken = getToken
function getToken (flag?:boolean){
  return Promise.resolve().then(()=>{
    if (flag){
      we_accToken.expireAt = 0
    }
    if ((!we_accToken.token) || (we_accToken.expireAt < Date.now() )){
      var options = {
      hostname: 'api.weixin.qq.com',
      path: `/cgi-bin/token?grant_type=client_credential&appid=${we_id}&secret=${we_secret}`,
      method: 'GET'}
      return Hps.httpsParser(options).then((httpsData:string)=>{
          return JSON.parse(httpsData)
        }).then((result)=>{
          if (result.errcode) {
            throw (new Error(result.errmsg))
          } else if (result.access_token) {
            we_accToken.token= result.access_token
            we_accToken.expireAt = (Date.now()) + result.expires_in * 1000
            return we_accToken.token
          } else {
            throw (new Error('No valid token'))
          }
        })
    } else{
      return we_accToken.token
    }
  })
}

// get token
app.queryDB = queryDB
function queryDB (options){
  return Promise.resolve().then(()=>{
      var httpOptions = {
      hostname: '218.104.220.115',
      port: 9995,
      path: `/dbQuery?type=${options.type}&keyword=${encodeURI(options.keyword)}&auth=XXXXXXXXXXXXXXXxxxxxxxxxxx&msgID=${Date.now()}`,
      method: 'GET'}
      return Hps.httpParser(httpOptions).then((httpsData:string)=>{
          return JSON.parse(httpsData)
        }).then((result)=>{
          if (result.errorCode) {
            throw (new Error("DBerror"))
          }else if (result.reply){
            return JSON.parse(result.reply)
          }else{
            return ""
          }
        })
    })
}

// server authentication
function wechatAuth(req, res, next) {
  //test bypass
  if (app.options.bypassWechatAuth || (req.query.auth === "XXXXXXXXXXXXXXXxxxxxxxxxxx")) {
    next() 
  }

  let q = req.query
  if (q.signature && q.timestamp && q.nonce) {
    let arr = [we_token, q.timestamp, q.nonce].sort().join('')
    let sha1 = crypto.createHash('sha1')
    let result = sha1.update(arr).digest('hex')
    if (result === q.signature) {
      if(q.echostr){
       res.end(q.echostr)
      }else{
        next()
      }
    } else {
      //  debug('Incoming invalid wechat message.')
      res.end()
    }
  } else {
    res.end()
  }
}

// Send message
app.sendMsg = sendMsg
function sendMsg(userId, msg){
  return getToken().then((token)=>{
      let msgOut = {
      touser:userId,
      msgtype:"text",
      text:{content:msg}
    }
      let postData = JSON.stringify(msgOut)
      let options = {
      hostname: 'api.weixin.qq.com',
      path: `/cgi-bin/message/custom/send?access_token=${token}`,
      method: 'POST',
      headers:{'Content-Length': Buffer.byteLength(postData)},
      postData: postData
    }
      return Hps.httpsParser(options)
    }).then((result)=>{
      // handle possible wechat message error
      return true
    }).catch((e)=>{
      debug("Exception when sending message. Error:",e)
    })
}

// Update menu

app.updateMenu = updateMenu
function updateMenu(newMenu:{}) {
  let menu = {
    "button": [
        {
            "type": "click", 
            "name": "课程查询Training Info", 
            "key": "menu_1"
        }, 
        {
            "name": "帮助信息Help", 
            "sub_button":[
              {
                "type": "click", 
                "name": "使用说明Instruction", 
                "key": "menu_2_1"
              }, 
              {
                "type": "click", 
                "name": "联系电话Contacts", 
                "key": "menu_2_2"
              }, 
              {
                "type": "click", 
                "name": "模拟机编号Sim certificates", 
                "key": "menu_2_3"
              }, 
          ]
        }
    ]
}
  debug("updateMenu: being called.")
  return Promise.resolve().then(()=>{
    // todo
  })
}




// helper for openid-id query


var findid = (()=>{
  var idCollection = {}
  return async function (openid:string) {
    if (idCollection[openid]) {
       return idCollection[openid]
    }
    else{
      debug("wechatlib findid: ", openid, " is not in cache ")
      let tmpid = ((await models.db.pool.get(`SELECT id FROM users WHERE openid = '${openid}'`)) || {}).id
      if(tmpid){
        idCollection[openid] = tmpid
        debug("wechatlib findid: ", openid, " belongs to ", tmpid)
        return tmpid
      }
      else{
        debug("wechatlib findid: ", openid, " is not in DB ")
        return undefined
      }
    }
}
})()

app.findid = findid
// Get user info
app.getUserInfo = getUserInfo
function getUserInfo (openid:string){
  return getToken().then((tokenResult)=>{
    var options = {
          hostname: 'api.weixin.qq.com',
          path: `/cgi-bin/user/info?access_token=${tokenResult}&openid=${openid}&lang=zh_CN`,
          method: 'GET'}
  return Hps.httpsParser(options).then((httpsData:string)=>{
      return JSON.parse(httpsData)
    }).then((result)=>{
      let tmp = {}
      if (result.errcode) {
         throw (new Error(result.errmsg))
      }
      else if(result.subscribe !== 0){
        tmp["nickname"] = result.nickname
        tmp['headimgurl'] = result.headimgurl
        tmp['openid'] = openid
        tmp['subscribe'] = 1
      }
      else{
        tmp['subscribe'] = 0
        tmp['openid'] = openid
      }
      return tmp
    }).catch((e)=>{
    debug("getUserInfo error:  ", e)
    })
  })
}

// Interface defs
interface Req {
    MsgType?,
    MsgId?,
    Event?,
    FromUserName?,
    CreateTime?
  }



// Central dispatcher
/*
switch (message || event)

*/
app.wxMsgKeys = ["ToUserName", "FromUserName", "CreateTime", "MsgType", "Event", "Content", "MsgId", "EventKey"]

async function dispatcher (req, res, next){
  return Hps.xmlParser(req.body).then(async (xmlObj)=>{
    let xmlReq:Req = xmlObj['xml'] || {}
    let iReq:Req = {}
    for (let key of app.wxMsgKeys){
        iReq[key] = (xmlReq[key]||[])[0]
     }
     debug(iReq)
    // validate request message structure
    if (!iReq.FromUserName||!iReq.MsgId && !iReq.CreateTime) {
       debug("No valid message information in request.", iReq)
      next()
    }

    // using MsgId (message) or FromUserName+CreateTime (Event) for deduplication
    iReq.MsgId = iReq.MsgId || (iReq.FromUserName + iReq.CreateTime)
    if (msgIds.has(iReq.MsgId)){
      debug("Duplicated message or no valid message id, just bypass. ID:", iReq.MsgId||"N/A")
      next()
     } else {
        msgIds.add(iReq.MsgId)
        setTimeout(function() {
          msgIds.delete(iReq.MsgId)
        }, 10000)
     }
    validHdlr(iReq)
    next()
  }).catch((e)=>{
    debug("Exception in dispatcher \n",e)
    res.end()
  })
}


async function validHdlr(iReq:Req) {
  let userId = await findid(iReq.FromUserName)
  let userModel:{textStatus?, attr?, save?} = {}
  if(!userId ) {
    let userinfo = (await getUserInfo(iReq.FromUserName)) || {}
    debug("validHdlr: ", userinfo)
    if(Object.keys(userinfo).length > 0){
      userModel = new models.modelList.users(userinfo)
      userModel.textStatus = 0
    }
  }
  else{
    userModel = await app.models.modelList.users.instances[userId]
  }
  // Update lastActive attr of user
  userModel.attr.lastActive = (new Date()).toISOString()

  switch(iReq.MsgType){
    case "text" : 
    textHdlr(iReq, userModel)  
    debug("validHdlr: text branch hit. ", iReq.MsgId)
    break
    case "event" :
    eventHdlr(iReq, userModel)
    debug("validHdlr: Event branch hit. ", iReq.MsgId)
    break
    case undefined:
    debug("validHdlr: No Msgtype in incoming request!")
    return false
    default:
    debug("validHdlr: Unsupport message type from user. ", iReq.MsgType)
  }
}


async function textHdlr(iReq, userModel) {

  // debug("textHdlr: User msg:", iReq.Content)
  // if (userModel.attr.verified === 0) {
  //   let regResult = app.regs.verify.exec(iReq.content)
  //    if (regResult) {
  //       let queryResult = await queryDB({type:"mobile", keyword:regResult[1]})
  //       if(!queryResult){

  //       }

  //    } else{
  //   sendMsg(userModel.attr.openid, "对不起，您的输入不正确，")
    
  //    }
  // } else{
  //   sendMsg(userModel.attr.openid, "您已通过身份验证，请使用底部菜单进行查询。")
  // }

  // if (userModel.attr.multiStepStatus) {

  // }
  switch (iReq.Content) {
    // case "re":
    //   queryDB({type:"maint", keyword:""}).then(async (queryRlt)=>{
    //     let tmp:[string] = ["近期维护安排："]
    //     for(let k of queryRlt){
    //       tmp.push(`${k.OPSRESBASENAME}: From ${moment(k.BEGINTIME)} to  ${moment(k.ENDTIME)} `)
    //     }
    //     tmp.push("查询完毕。")
    //     if(tmp.length ===<number>2){
    //       sendMsg(userModel.attr.openid, `对不起，没有查询到您近期的课程记录。`)
    //     }else{
    //       for(let k of tmp){
    //         sendMsg(userModel.attr.openid, k)
    //         await Hps.sleep(200)
    //       }
    //     }
    //   }).catch((e)=>{
    //     debug(e)
    //     sendMsg(userModel.attr.openid, `对不起，查询出错，请稍候重试。`)
        
    //   })
    //   break;
  
    default:
      if (app.options.blasklist.includes(iReq.Content) && app.options.enableBlasklist) {
         sendMsg(userModel.attr.openid,"抱歉，无法直接查询中心教员课程。")
         break
      }
      queryDB({type:"task", keyword:iReq.Content.toUpperCase()}).then(async (queryRlt)=>{
        let tmp:[string] = ["您近期的课程信息如下："]
        queryRlt.sort((a,b)=>{
          return Date.parse(a.BEGINTIME)-Date.parse(b.BEGINTIME)
        })
        let replyString = ""
        for (let k of queryRlt){
          let tmpString = JSON.stringify(k)
          if (!RegExp(iReq.Content.toUpperCase()+app.regs.name).exec(tmpString)
              || k.PLACES === null) {``
              continue
          }
          replyString += `课程名：${k.TRMCLASSTASKNAME}\n`
          if (!(k.TRMCOURSEPKGNAME||"").includes("干租")) {
            replyString += `课程类型：${k.TRMCOURSEPKGNAME}\n`
          }
          replyString += `日期：${moment(k.BEGINTIME).format('MMM D')}日\n`
          replyString += `时间：${moment(k.BEGINTIME).format('HH:mm')}-${moment(k.ENDTIME).format('HH:mm')}\n`
          replyString += `教员：${k.INSTRUCTORS || "未指定"}\n`
          replyString += `学员：${k.TRAINEES || "未指定"}\n`
          replyString += `设备：${k.PLACES}`
          tmp.push(replyString)
          replyString = ""
        }
        tmp.push("查询完毕。")
        if(tmp.length === <number>2){
          sendMsg(userModel.attr.openid, `对不起，没有查询到您近期的课程记录。`)
        }else{
          for(let k of tmp){
            sendMsg(userModel.attr.openid, k)
            await Hps.sleep(200)
          }
        }
    
      }).catch((e)=>{
        debug(e)
        sendMsg(userModel.attr.openid, `对不起，查询出错，请稍候重试。`)
        if (!app.dataServerAvail) {
           app.dataServerAvail = false
          // todo
        }
        
      })
      break
  }
  
  
}

async function eventHdlr(iReq, userModel) {
  switch(iReq.Event){
    case "CLICK":
      switch(iReq.EventKey){
        case "menu_1":
          sendMsg(iReq.FromUserName, `功能建设中，请直接发送姓名进行查询。`)
          break
        case "menu_2_1":
          sendMsg(iReq.FromUserName, '本系统的查询范围为自查询之日起未来十个日历日的模拟机训练计划，您可以直接回复自己的姓名获得查询结果。\n'
          +'本系统的姓名信息与飞行准备网一致，若您的姓名后跟有字母，请在回复您的姓名时添加正确的字母（不区分大小写）。\n'
          +'若在查询期内，您有多组训练，查询结果按场次日期的先后顺序排列。\n'
          +'本系统尚在测试阶段，查询结果仅供参考，请以培训中心发布的课表及短息通知为准。')
          break
        case "menu_2_2":
          sendMsg(iReq.FromUserName, '协调值班电话：18980471356\n'
          +'工程师值班电话：67896999\n'
          +'启航楼前台电话：67896000\n'
          +'模拟机观摩申请电话：67896658（工作日9:00-17:00）')
          break
        case "menu_2_3":
          sendMsg(iReq.FromUserName, `模拟机编号：
FFS#1 FSD-105
FFS#2 FSD-114
FFS#3 FSD-161
FFS#4 FSD-199
FFS#5 FSD-246`)
          break
        default:
        debug('EventHdlr: unknowed menu key...')
        break
      }
      break
    case "subscribe":
      await Hps.sleep(500)
      sendMsg(iReq.FromUserName, '您好，欢迎关注，本系统提供飞行培训计划短期查询。\n'
      +'本系统尚处于内测阶段，查询结果仅供参考，训练计划信息请以培训中心发布的课表及短信通知为准。\n'
      +'点击帮助信息可以查看具体使用说明、联系电话和模拟机编号。')
      return
    case "unsubscribe":
      userModel.attr.subscribe = 0
      return
    default:
      debug('Event Hdlr: ', "Some unhandled event")
      return
  }
  debug("Event handler: ", iReq.Event, " - ", iReq.EventKey)
}
// logger
function logger (req, res, next){
  debug(`No. ${incomingCnt++} @${(new Date()).toLocaleString('zh-CN',{hour12:false})}`)
  next()
}

// Generic error handler
function errHdlr (err, req, res, next) {
  debug("Express errHdlr: ", err.message)
  res.sendStatus(500)
}

// Finalizer
function finalizer (req,res,next) {
  debug("Express Finalizer: finalized.")
  res.end()
}



// Generate message

app.msgGen = msgGen


function msgGen(code: string, user = null, lang = "cn") {
  debug("msgGen: being called.")
  return Promise.resolve().then(()=>{
    
  })
}

function textGen(user, title) {

  
}



// unused functions
// Helper to set patrols at certain time

async function shceduler() {
  let now = moment().millisecond(0)
  let pTime = now.hour()<10 ? now.clone().hour(10).minute(0).second(0).diff(now):now.clone().add(1,'day').hour(10).minute(0).second(0).diff(now)
  let t1 = setTimeout(remindUser, pTime);
  debug("reminder timer id: ", t1)
  pTime = now.hour()<23 ? now.clone().hour(23).minute(0).second(0).diff(now):now.clone().add(1,'day').hour(23).minute(0).second(0).diff(now)
  let t2 = setTimeout(checkOverdue, pTime);
  debug("checkOverdue timer id: ", t2)
}



// Inspect for due alarming

async function patrolUsers(checker) {
  if (app.models.db.sync) {
     return app.models.db.pool.all("SELECT users.id, users.openid, users.currentGoal,users.subscribe, users.lastActive, goals.content, goals.due FROM users,goals WHERE users.currentGoal = goals.id").then((results)=>{
      debug("PatrolUsers: total user with valid goal: ", results.length)
      let resultArray = []
      for (let result of results){
        if(checker(result)){
          resultArray.push(result)
        }
      }
      debug("Patrolusers: result:", resultArray)
      return resultArray
    }).catch((e)=>{
     debug("Patrolusers: failed. ")
    })
  }
}








// Remind user

async function remindUser() {
  setTimeout(remindUser, 86400000);
  let resultArray = await patrolUsers((result)=>{
    return moment(result.due).diff(moment(), 'days') === 0 ? true: false
  })
  if (Object.prototype.toString.apply(resultArray) !== '[object Array]') {
    return
 }
  for (let result of resultArray){
    sendMsg(result.openid, `Your current goal:\n ${result.content} \n is due today. Please check your goal status.`)
  }
    resultArray = await patrolUsers((result)=>{
    return moment(result.due).diff(moment(), 'days') === 1 ? true: false
  })
  if (Object.prototype.toString.apply(resultArray) !== '[object Array]') {
    return
 }
  for (let result of resultArray){
    sendMsg(result.openid, `Your current goal:\n ${result.content} \n is due tomorrow. Please check your goal status.`)
  }
  resultArray = await patrolUsers((result)=>{
    return moment(result.due).diff(moment(), 'days') === 5 ? true: false
  })
  if (Object.prototype.toString.apply(resultArray) !== '[object Array]') {
    return
 }
  for (let result of resultArray){
    sendMsg(result.openid, `Your current goal:\n ${result.content} \n is due in 5 days. Please check your goal status.`)
  }
}

// Remind user

async function checkOverdue() {
  setTimeout(checkOverdue, 86400000);
  let resultArray = await patrolUsers((result)=>{
    return moment(result.due).diff(moment()) < 5000 ? true: false
  })
  if (Object.prototype.toString.apply(resultArray) !== '[object Array]') {
    return
 }
  for (let result of resultArray){
    let userModel = await app.models.modelList.users.instances[result.id]
    let goalModel = await app.models.modelList.goals.instances[result.currentGoal]
    await userModel.setNull("currentGoal")
    goalModel.attr.status = -1
    sendMsg(result.openid, `Your current goal:\n ${result.content} \n is overdue and your can set new goal now.`)
  }
}