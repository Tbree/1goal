// Custom error definitions

// DB related errors
// 1. Init Error
export class dbInitError extends Error{
  constructor(message){
    super()
    this.name = "dbInitError"
  }
}