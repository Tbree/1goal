// Customize debug output for this lib
import {debugGen} from './helpers'
const debug = debugGen('1Goal.model.connector.Sqlite3')

// Requirement
import * as sqlPoolLib from 'sqlite-pool'

// Export object
export default class sqlite3DB {
  // Properties declaration
  public pool:sqlPoolLib
  public sync:boolean
  public tables:{} = {}
  public retries:number
  public inspector //should be :NodeJS.Timer, for some reason VScode doesn't handl this correctly.

  // Method declaration
  constructor (dbPath:string = '../db/database.sqlite3', options:{} = {max:1}){
    this.pool = new sqlPoolLib(dbPath, options)
    this.sync = false
    this.retries = 0
    this.inspector = setInterval(this.checkDB.bind(this), 1000)
  }

  public init(){
    return this.pool.all("SELECT name FROM sqlite_master WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%' ORDER BY 1")
      .then((tables:{name:string}[])=>{
        let tmp = []
        this.tables = {}
        for (let table of tables){
          tmp.push (this.pool.all(`PRAGMA table_info(${table.name})`).then((tuples)=>{
            let attrProto = {}
            Object.defineProperties(attrProto, {
              'notNull':{
                writable:true,
                enumerable:false,
                value: []
              },
              'primary':{
                writable:true,
                enumerable:false,
                value: []
              }
            })
            for (let tuple of tuples){
              attrProto[tuple.name] = null
              if (tuple.notnull === 1) {
                 attrProto['notNull'].push(tuple.name)
              }
              if (tuple.pk === 1) {
                 attrProto["primary"] = tuple.name
              }
            }
            debug('DBinit: Saving table:', table.name )
            this.tables[table.name] = attrProto
          }))
        }
        return Promise.all(tmp)
      }).then(()=>{
        this.retries = 0
        debug('DB init Finished')
        this.sync = true
        return this.pool
      })
  }

  public checkDB(forceFlag?:boolean){
    if (this.sync === true && !forceFlag) {
      return true 
    }
    this.pool.all("SELECT name FROM sqlite_master WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%' ORDER BY 1")
      .then(()=>{
        debug("Checking DB succeed.")
        this.sync = true
        return true
      })
      .catch((e)=>{
        debug("Checking DB failed.")
        this.sync = false
        return false
    }) 
  }

  
}
