// Requirement
import * as events from 'events'
import sqlite3DB from './connector-sqlite3'


// Formated and numbered debug messages
import * as debugLib from 'debug'

export function debugGen(name:string = "TInfo.generic"){
  let _debug = debugLib(name)
  let _debugCnt = 0
  return function(...params){
    _debugCnt += 1
    params.unshift(`{${_debugCnt}} @ ${(new Date()).toLocaleString("zh-CN", {hour12:false})} :`)
    _debug.apply(this,params)
  }  
}
const debug = debugGen('TInfo.helpers')

// await version of delay function
export function sleep (ms: number){
  return new Promise((resovle)=> setTimeout(resovle, ms))
}

// xml parser helper
import * as xml2js from 'xml2js'

export function xmlParser(xml:string){
  return new Promise((resolve, reject)=>{
    if(Object.keys(xml || {}).length === 0){
      resolve("")
    }
    xml2js.parseString(xml, (err,result)=>{
      err? reject(err):resolve(result)
    })
  })
  .catch((e)=>{debug('XML parse error.')})
}

// https parser helper
import * as https from 'https'

export function httpsParser (options){
  return new Promise((resolve, reject)=>{
    var req = https.request(options,function(res){
      var result = ""

      res.on('data',(d)=>{
        result = result + d
      });

      res.on('end',()=>{
          debug("https result: ", result)
          resolve(result)
      });
    });

    req.on('error',(e)=>{
      reject(e);
    })

    if (options.method === "POST"){
      req.write(options.postData)
    }
    req.end();
  });
};

// http parser helper
import * as http from 'http'

export function httpParser (options){
  return new Promise((resolve, reject)=>{
    var req = http.request(options,function(res){
      var result = ""

      res.on('data',(d)=>{
        result = result + d
      });

      res.on('end',()=>{
          debug("https result: ", result)
          resolve(result)
      });
    }); 
    req.setTimeout(4000, ()=>{
      reject(new Error("Http timeout"))
      req.abort()
    })
    req.on('error',(e)=>{
      reject(e)
    })

    if (options.method === "POST"){
      req.write(options.postData)
    }
    req.end()
  });
};

// I18n helper

export function i18n(msgID:string, lang:string = "zh-cn") {
  
}

