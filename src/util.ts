let debug = require('debug')("1goal.util")
const http = require('http')
const https = require('https')


export let httpParser = function(options:{path?:string; hostname?:string; port?:number; method?:string; postData?:string; headers?:any } = {}){
  return new Promise((resolve, reject)=>{
    options.path = options.path || "/?signature=1de82f0ab7325d5c7851e21f9c44f98d1e218253&timestamp=1488596439&nonce=1643897633&openid=ois-5s7zctSaAgzG0ROk3S0epALE"
    options.hostname = options.hostname ||  "127.0.0.1"
    options.port = options.port || 3001
    options.method = (options.method === "POST") ? "POST":"GET"
    options.headers = {
      'Content-Type': 'text/xml',
      'Content-Length': Buffer.byteLength(options.postData||"")
    }
    console.log(options)
    var req = http.request(options,function(res){
      var result = ""

      res.on('data',(d)=>{
        result = result + d
      });

      res.on('end',()=>{
          resolve(result)
      });
    });

    req.on('error',(e)=>{
      reject(e);
    })

    if (options.method === "POST"){
      req.write(options.postData)
    }
    req.end();
  });
};

