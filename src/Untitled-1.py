import fileinput
import sys
import os
import re

deltaDate = 

def findAll(inputDir):
  print "entering folder: ", inputDir
  for element in os.listdir(inputDir):
    if os.path.isdir(os.path.join(inputDir,element)):
      findAll(os.path.join(inputDir,element))
    elif re.search("_smp.tsf$", element):
      print "Target file: ", os.path.join(inputDir,element)
    else:
      print "Delete unwanted file: ", element
      os.remove(os.path.join(inputDir,element))






def replaceAll(file,searchExp,replaceExp):
    for line in fileinput.input(file, inplace=1):
        if searchExp in line:
            line = line.replace(searchExp,replaceExp)
        sys.stdout.write(line)



