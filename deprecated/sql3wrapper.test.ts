import test from 'ava'

async function sleep (ms: number){
  return new Promise((resovle)=> setTimeout(resovle, ms))
}

//sql3Wrapper test cases

import {sql3Wrapper as sql3} from './sql3Wrapper'
var sqlite3 = require('sqlite3')
var debug = require('debug')('test.sql3Wrapper')
var sqldb: sql3

test.before(async t=>{
  sqldb = new sql3()
  while (!sqldb.db.open){
    await sleep(100)
  }
  debug('db is ready')
})

test.serial('case 1: invalid params', async t =>{
  let sqlstring = 1;
  t.throws(sqldb.run(sqlstring))
})

test.serial('case 2: create table', async t=>{
  let sql1={sql : "CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT default('用户'), wechat TEXT, portrait TEXT, lastAction TEXT DEFAULT(DATETIME('now')), created TEXT DEFAULT(DATETIME('now')), modified TEXT DEFAULT(DATETIME('now')))"}
  let sql2={sql : "CREATE TRIGGER IF NOT EXISTS update_time_users after UPDATE ON users FOR EACH ROW BEGIN UPDATE users SET modified = DATETIME('now') WHERE rowid = OLD.rowid; END;"}
  let sql3={sql : "CREATE TABLE IF NOT EXISTS goals (id INTEGER PRIMARY KEY AUTOINCREMENT, userid INTEGER, goal TEXT, status INTEGER, deadline TEXT DEFAULT(DATETIME('now','+1 day')), created TEXT DEFAULT(DATETIME('now')), modified TEXT DEFAULT(DATETIME('now')))"}
  let sql4={sql : "CREATE TRIGGER IF NOT EXISTS update_time_goals after UPDATE ON goals FOR EACH ROW BEGIN UPDATE goals SET modified = DATETIME('now') WHERE rowid = OLD.rowid; END;"}
  try{
    await sqldb.run(sql1)
    await sqldb.run(sql2)
    await sqldb.run(sql3)
    await sqldb.run(sql4)
  }
  catch(e){
    debug(e)
    t.fail()
  }
  t.pass()
})

test.serial('case 3: insert values',async t=>{
  let sql = {
    sql: "insert into users",
    params: ""
  }
})