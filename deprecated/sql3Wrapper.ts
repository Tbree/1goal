import * as debugGen from 'debug'
let debug = debugGen('sql3Wrapper')
import * as sql3 from 'sqlite3'

/**
 * Interface declaration
 */


interface sql3WrapperInterface {
  connect: (dbPath?:string) => Promise<sql3.Database>
  run: (sqlScript: sqlString) => Promise<sql3.Database>
  all: (sqlScript: sqlString) => Promise<any[]>
  get: (sqlScript: sqlString) => Promise<any[]>
  close:()=>void
}

enum sqlMethod {
  insert,
  update
}

interface sqlString {
  sql: string
  params?:any[]
}

interface hEntry{
  results?:any[]
  length?:number
}

export class sql3Wrapper implements sql3WrapperInterface {
  constructor (dbPath?:string){
    this.dbPath = dbPath || "\database.sqlite3"
    this.connect().then((thedb:sql3.Database)=>{
      this.db = thedb
    })
    .catch((err:any)=>{
      debug('Error encounterred while connecting:')
      debug(err.message)
      this.db = undefined
    })
  }
  
  db:sql3.Database|undefined = undefined
  private dbPath:string
  public runHistroy:{}[]=[]

  connect(dbPath?:string) {
    return new Promise((resolve, reject) => {
      if (!this.db || dbPath) {
        this.dbPath = dbPath||this.dbPath
        this.db = sql3.cached.Database(this.dbPath, (err:Error) =>{
          if (err) {
            reject(new Error('Failed to connect to DB'));
          } else {
            resolve(this.db);
          }
        })
      }
      else {
        resolve(this.db);
      }
    })
  }

run(sqlString:sqlString){
  var dbpointer = this
  return new Promise((resolve,reject)=>{
    this.db.run(sqlString.sql,sqlString.params,function(err){
      if(err){
        reject(err)
      }
      else{
        resolve({
          date:Date.now(),
          sql:sqlString.sql,
          lastID:this.lastID,
          changes:this.changes
        })
      }
    })
  }
  ).then((historyEntry:hEntry)=>{
    dbpointer.runHistroy.push(historyEntry)
    return this.db
  })
}

all(sqlString:sqlString){
  var dbpointer = this
  return new Promise((resolve,reject)=>{
    this.db.all(sqlString.sql,sqlString.params,function(err,rows){
      if(err){
        reject(err)
      }
      else{
        resolve({
          results:rows,
          date:Date.now(),
          sql:sqlString.sql,
        })
      }
    })
  }
  ).then((historyEntry:hEntry)=>{
    let h1 = historyEntry
    delete h1.results
    h1.length = historyEntry.results.length
    dbpointer.runHistroy.push(h1)
    return historyEntry.results
  })
}

get(sqlString:sqlString){
  var dbpointer = this
  return new Promise((resolve,reject)=>{
    this.db.get(sqlString.sql,sqlString.params,function(err,row){
      if(err){
        reject(err)
      }
      else{
        resolve({
          results:row,
          date:Date.now(),
          sql:sqlString.sql,
        })
      }
    })
  }
  ).then((historyEntry:hEntry)=>{
    dbpointer.runHistroy.push(historyEntry)
    return historyEntry.results
  })
}

close(){
  this.db.close()
}

static cookParams(attrObj:{}, method:sqlMethod){

}
}
