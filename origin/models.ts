import * as debugLib from 'debug'
var debug = debugLib('1Goal.ORM_SQLITE3')
import * as sqlPoolLib from 'sqlite-pool'

// prepare parameters helper
function prepareParams(attrObj, method){
  if (typeof attrObj !== "object"){
    throw new TypeError("Expect an object");
  }
  attrObj = (!!attrObj.attr)? attrObj.attr : attrObj;
  let sqlString1 = "";
  let sqlString2 = "";
  let params = [];
  for (let key of Object.keys(attrObj)){
    if (attrObj[key]){
    sqlString1 += (key+",");
    sqlString2 +="?,";
    params.push(attrObj[key]);
    }
  }
  if (sqlString1 === ""){
    throw new Error("No valid parmas");
  }
  sqlString1 = "("+sqlString1.slice(0,-1)+")";
  sqlString2 = "("+sqlString2.slice(0,-1)+")";
  if (method === "update"){
    return [sqlString1+ "=" +sqlString2, params];
  }
  return [sqlString1+ "VALUES" +sqlString2, params];
}


// factory function fo class (contructor)
function factory(className:string,attrProto:{}, ref:jDB){
  let _name = className
  let _attrProto = attrProto
  let _ref = ref
  let tmpFn:any = function (outAttrObj = {}){
    this.name = _name
    this._attr = {}
    for (let key of Object.keys(_attrProto)){
      this._attr[key] = outAttrObj[key]
    }
    this.ref = ref
    if (this._attr.id){
      tmpFn.instances[this._attr.id] = this
    }
    // this.save =  ()=>{
    //   return Promise.resolve().then(()=>{
    //     let tmpObj = Object.assign({},_attrProto)
    //     for (let key of Object.keys(tmpObj)){
    //       tmpObj[key] = this._attr[key]
    //     }
    //     let method = this._attr.id? "update" : "insert"
    //     let params = prepareParams(tmpObj,method)
    //     if (method === "update"){
    //       params[0] = `UPDATE ${_name} SET ` + params[0] + ` where id = ${this._attr.id}`
    //     } else{
    //       params[0] = `INSERT INTO ${_name} ` + params[0]
    //     }
    //     return this.ref.pool.run(params[0],params[1])
    //   })
    // }
    this.close = ()=>{
      if (this._attr.id){
        delete tmpFn.instances[this._attr.id]
        return true
      }
    }
  }
  tmpFn.instances = {}
  tmpFn.find = (query:string|number)=>{
    return Promise.resolve().then(()=>{
      if (tmpFn.instances[query]){
        return tmpFn.instances[query]
      }
      let sqlString = `SELECT * FROM ${_name} WHERE `
      let sqlParam = [query]
      switch (typeof query)
      {
        case "string": 
        sqlString += "name = ?"
        break
        case "number":
        sqlString += "id = ?"
        break
        default:
        throw new Error("Invalid query type, should be String or Number")        
      }
      return _ref.pool.all(sqlString,sqlParam).then((results:{id:number}[])=>{
        let queryResults = []
        for(let result of results){
          tmpFn.instances[result.id] = new tmpFn(result)
          queryResults.push(tmpFn.instances[result.id])
        }
        return queryResults
     })
    })
  }
  tmpFn.query = (query:string)=>{
    return Promise.resolve().then(()=>{
      return _ref.pool.run(query)
    })
  }
  tmpFn.close = ()=>{
    tmpFn.instances = {}
    return true
  }
  return tmpFn
}


/**
 * 
 */
/**
 * Interface declaration
 */

export class jDB {
  // object stores models represent each table from this database
  public Models:any = {}
  public pool:sqlPoolLib
  public ready:boolean = false
  public checker:NodeJS.Timer

  constructor (dbPath:string = './database.sqlite3', options:{} = {max:1}){
    this.pool = new sqlPoolLib(dbPath, options)
  }

  init(){
    return this.pool.all("SELECT name FROM sqlite_master WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%' ORDER BY 1").then((tables:{name:string}[])=>{
      for (let table of tables){
        this.pool.all(`PRAGMA table_info(${table.name})`).then((tuples)=>{
          let attrProto = {}
          for (let tuple of tuples){
            attrProto[tuple.name] = null
          }
          this.Models[table.name] = factory(table.name, attrProto ,this)
        }).catch((e)=>{
          this.ready =false
          debug(e.message, "--------inside")
        })
      }
      this.ready = true
      this.checker = setInterval(()=>{
        this.pool.run("select * from sqlite_master")
          .then(()=>{this.ready = true})
          .catch((e)=>{
            this.ready = false
            debug("jDB not ready yet. retry in next tick")
          })
      },800);
    })
  }

  close(){
    return this.pool.close().catch(e=>{debug(e)})
  }

} 
