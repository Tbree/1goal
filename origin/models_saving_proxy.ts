// Customize debug output for this lib
import {debugGen} from './helpers'
const debug = debugGen('1Goal.model.generic')

// Requirement
import * as Hps from './helpers'
import * as CustomErr from './ErrorDef'
import sqlite3DB from './connector-sqlite3'
import * as events from 'events'

// export
export default class Models{
  public type:string
  public options:{}
  public db:sqlite3DB
  public entHdl:events
  public modelList:any

  constructor(options:any={db:'sqlite3'}){
    switch (options.db){
      case undefined:
      case 'sqlite3':
        this.type =  options.db
        this.db = new sqlite3DB(options.path, options)
      break
      case 'postgre':
      case 'redis':
      case 'mongo':
      default:
      throw(new TypeError('DB not supported.'))
    }

  }

  async init(){
    return Promise.resolve().then(async ()=>{
      if ((this.db.sync === true) || (this.db.retries < 10 && await this.db.checkDB(true))) {
      debug("Model init: start init.")
        this.modelList = {}
        //Model setting init
        return this.db.init().then(()=>{
          this.db.retries = 0
          for (let key of Object.keys(this.db.tables)){
          this.modelList[key] = classFactory(key, this.db.tables[key],this.db)
          }
        })
      }
      else{
        debug("Model init: failed, retry. No. ", this.db.retries)
        this.db.retries +=1
        await Hps.sleep(200)
        return this.init()
      }
    })
  }
}



// prepare parameters helper
function prepareParams(attrObj, method){
  if (typeof attrObj !== "object"){
    throw new TypeError("Expect an object");
  }
  attrObj = (!!attrObj.attr)? attrObj.attr : attrObj;
  let sqlString1 = "";
  let sqlString2 = "";
  let params = [];
  for (let key of Object.keys(attrObj)){
    if (key === "id") {
      continue 
    }
    if (attrObj[key]){
    sqlString1 += (key+",");
    sqlString2 +="?,";
    params.push(attrObj[key]);
    }
  }
  if (sqlString1 === ""){
    throw new Error("No valid parmas");
  }
  sqlString1 = "("+sqlString1.slice(0,-1)+")";
  sqlString2 = "("+sqlString2.slice(0,-1)+")";
  if (method === "update"){
    return [sqlString1+ "=" +sqlString2, params];
  }
  return [sqlString1+ "VALUES" +sqlString2, params];
}

// factory function fo modele class contructor
function classFactory(className:string,attrProto:{notNull?}, dbRef:sqlite3DB){
  // counter for minus id number of newly created instances
  let _counter = 0

  // eventer for automatic saving
  let _event = new events()
  let savingHdlr = async function(id:number){
    debug("DB: saving handling ", className, id)
    if ((dbRef.sync === false)||!(id && classProduct._instances[id])) {
      debug("DB: saving handling aborted, DB not sync or no valid ID provided")
       return false
    }
    for (let notNullKey of attrProto.notNull){
       if (!classProduct._instances[id].attr[notNullKey]) {
          debug(notNullKey, " must not be null.")
          return
       }
     }
    await Hps.sleep(300)
    try {
      await Promise.resolve().then(()=>{
        let tmpObj = {}
        for (let key of Object.keys(classProduct._instances[id]._attr)){
          tmpObj[key] = classProduct._instances[id]._attr[key]
        }
        let method = (classProduct._instances[id]._attr.id > 0) ? "update" : "insert"
        let params = prepareParams(tmpObj, method)
        if (method === "update"){
          params[0] = `UPDATE ${className} SET ` + params[0] + ` where id = ${classProduct._instances[id]._attr.id}`
        } else{
          params[0] = `INSERT INTO ${className} ` + params[0]
        }
        return dbRef.pool.run(params[0],params[1])
      })
      .then(async (returnObj:{stmt:{lastID?:number, changes?:number, sql?:string}})=>{
        debug("SQL statement completed:",returnObj.stmt.sql, "@", returnObj.stmt.lastID, "/", returnObj.stmt.changes)
        classProduct._instances[id].savingWaiting = false
        // Update ID property and instances ref of newly inserted record with returned ID
        if (id < 0) {
          classProduct._instances[id]._attr.id = returnObj.stmt.lastID
          classProduct._instances[returnObj.stmt.lastID] = classProduct._instances[id]
          delete classProduct._instances[id]
          // classProduct._instances[returnObj.stmt.lastID].refresh()
        }
        else{
          // classProduct._instances[id].refresh()
        }
        
      })
    }
    catch (err){
      debug(`DB operation: saving failed! [${classProduct._instances[id]._attr.name}.${classProduct._instances[id].attr.id}]`)
      classProduct._instances[id].savingWaiting = false
      return false
    }
    return true
  }

  _event.on('Changed',savingHdlr)

  //
  let classProduct:any = function (outAttrObj= {}, id:number = null){
    // !!! Return an empty array if id is provided in outAttrObj, it should be given in id property if necessary.
    // A check means should be introduced to check the output.
    if (outAttrObj["id"]) {
      debug('id must be a separate second para. Return empty array.')
       return []
    }
    // Check notnull keys
    // A check means should be introduced to check the output.
    for (let notNullKey of attrProto.notNull){
       if (!outAttrObj[notNullKey]) {
          debug(notNullKey, " must not be null. Return empty array.")
          return []
       }
     }

    this.name = className
    this._attr = {}
    this.savingWaiting = false
    
    // filter out keys not in own attr object
    for (let key of Object.keys(attrProto)){
      this._attr[key] = outAttrObj[key] || null
    }
    // set id to given id or an automatic minus one
    this._attr.id = id ? id: (_counter-=1)
    
    classProduct._instances[this._attr.id] = this

    // Set proxy for automatic saving
    let setHdlr = (obj, prop, value)=>{
      if (obj[prop]!==undefined) {
        debug(`DB: setHdrl: old/new - ${obj[prop]}/${value}`)
        obj[prop] = value
        if (!this.savingWaiting) {
          debug("DB: setHdlr: setauto saving ",this.name, ' ', this._attr.id)
          this.savingWaiting = true
          _event.emit("Changed", obj.id)
        }else{
          debug("DB: autosaving suppressed, savingWaiting is true. " ,this.name, ' ', this._attr.id)
        }
        return true
      }
      return false
    }
    this.attr = new Proxy(this._attr,{set: setHdlr})

    this.close = ()=>{
        delete classProduct._instances[this._attr.id]
      }

    this.save = ()=>{
      savingHdlr(this._attr.id)
    }

    this.refresh = ()=>{
      debug("DB refresh: refreshing attrs.")
      return dbRef.pool.get(`SELECT * FROM ${this.name} WHERE id = ${this.attr.id}`).then((result)=>{
        for (let key of Object.keys(attrProto)){
           this._attr[key] = result[key]
         }
      }).catch((e)=>{
        debug('DB refresh: failed')
      })
    }

    setTimeout(savingHdlr, 200, this._attr.id);
    }

  // Static class properties
  classProduct._instances = {}
  classProduct.db = dbRef

  // Intercept instances object get request to support dynamic loading
  // 1. Directly from cache;
  // 2. Query the DB for requested id
  let instanceHdlr = (target, name:string)=>{
    return Promise.resolve().then(()=>{
      if ( Object.prototype.toString.apply(name) !== '[object String]'){
        return classProduct._instances[name]
      }
      var id = parseInt(name)
      if ( isNaN(id)){
        return classProduct._instances[name]
      }
      if (classProduct._instances[id]) {
        debug(" cached instance: ", id)
         return classProduct._instances[id]
      }
      else{
        debug(" query db for instance: ", id)
        return dbRef.pool.get(`SELECT * FROM ${className} WHERE ID = ${id}`).then((result)=>{
          if (result) {
            delete result.id
            classProduct._instances[id]= new classProduct(result, id)
            return classProduct._instances[id]
          }
          else{
            debug("DB-instanceHdlr: no such id")
            return null
          }
        }).catch((e)=>{
            debug("DB-instanceHdlr: error")
            return undefined
      })
      }
    })
  }

  classProduct.instances = new Proxy(classProduct._instances, {get: instanceHdlr})

  classProduct.byAttr = {}
  for (let key of Object.keys(attrProto)){
    classProduct.byAttr[key] = (keyWord:number|string, unique?:boolean)=>{
      return Promise.resolve().then(()=>{
        let resultTmp = []
        let keySet = new Set()
        for (let instanceKey of Object.keys(classProduct._instances)){
          if (classProduct._instances[instanceKey]['attr'][key] === keyWord) {
            resultTmp.push(classProduct._instances[instanceKey])
            keySet.add(instanceKey)
          }
        }
        if (unique && (keySet.size > 0)) {
           return resultTmp
        }
        return dbRef.pool.all(`SELECT * FROM ${className} WHERE ${key} = '${keyWord}'`).then((results)=>{
          for (let result of results) {
            if (!keySet.has(result.id)) {
              let id = result.id
              delete result.id
              resultTmp.push(new classProduct(result, id))
            }
          }
          return resultTmp
      }).catch((e)=>{
        debug("byAttr of ", key, " failed while querying DB.")
    })
    }).catch((e)=>{
      debug("byAttr of ", key, " failed while sync process (not db).")
  })
  }
  }



  return classProduct
}




