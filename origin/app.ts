// Lib import
import {jDB as dbLib} from "./models"
const db = new dbLib('./db/database.sqlite3')
db.init().then(()=>{
  if(db.Models.users && db.Models.goals){
    debug(db.Models)
    return
  }else{
    return db.pool.run("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT default('用户'), wechat TEXT, portrait TEXT, status INTEGER, lastAction TEXT DEFAULT(DATETIME('now')), created TEXT DEFAULT(DATETIME('now')), modified TEXT DEFAULT(DATETIME('now')))")  
  }
}).then(()=>{
  return db.pool.run("CREATE TRIGGER IF NOT EXISTS update_time_users after UPDATE ON users FOR EACH ROW BEGIN UPDATE users SET modified = DATETIME('now') WHERE rowid = OLD.rowid; END;")
}).then(()=>{
  return db.pool.run("CREATE TABLE IF NOT EXISTS goals (id INTEGER PRIMARY KEY AUTOINCREMENT, userid INTEGER, goal TEXT, status INTEGER, deadline TEXT DEFAULT(DATETIME('now','+1 day')), created TEXT DEFAULT(DATETIME('now')), modified TEXT DEFAULT(DATETIME('now')))")
}).then(()=>{
  return db.pool.run("CREATE TRIGGER IF NOT EXISTS update_time_goals after UPDATE ON goals FOR EACH ROW BEGIN UPDATE goals SET modified = DATETIME('now') WHERE rowid = OLD.rowid; END;")
}).then(()=>{
  return db.init()
}).catch((e)=>{
  debug(e.message, "------end--------")
  process.exit()
})
import * as debugLib from 'debug'
const debug = (function(){
  let _debug = debugLib('1Goal.app')
  let _debugCnt = 0
  return function(...params){
    _debugCnt += 1
    params.unshift(`{${_debugCnt}}@ ${(new Date()).toLocaleString("zh-CN", {hour12:false})} :`)
    _debug.apply(this,params)
  }
  
})()
import * as https from 'https'
import * as crypto from 'crypto'
import * as express from 'express'
import * as parser from 'body-parser'
import * as helmet from 'helmet'
import * as xml2js from 'xml2js'
const xmlParser = (xml:string)=>{
  return new Promise((resolve, reject)=>{
    if(Object.keys(xml || {}).length === 0){
      resolve("")
    }
    xml2js.parseString(xml, (err,result)=>{
      err? reject(err):resolve(result)
    })
  }).catch(()=>{})
}

// await version of delay function
function sleep (ms: number){
  return new Promise((resovle)=> setTimeout(resovle, ms))
}

// Global vars
// 1. Incoming requests counter
var incomingCnt = 1
// 2. Messageid set for duplication check
var msgIds = new Set()

var app = express()

// Wecaht APP setting
const we_token = 'wangou'
const we_secret = '73d5bbbde6804a0766b17cccf70f67fb'
const we_id = 'wx97b2219c6b2a08d3'
const we_AESKey = 'baLNGr8LwPZREeqnMq2tAke6NEMvbTfswyljaIeeg8J'
let we_accToken = {
  token: "",
  expireAt: 0
}

// Generic helpers
app.httpsParser = function(options){
  return new Promise((resolve, reject)=>{
    var req = https.request(options,function(res){
      var result = ""

      res.on('data',(d)=>{
        result = result + d
      });

      res.on('end',()=>{
          debug(result)
          resolve(result)
      });
    });

    req.on('error',(e)=>{
      reject(e);
    })

    if (options.method === "POST"){
      req.write(options.postData)
    }
    req.end();
  });
};

app.getToken = function(flag:boolean){
  return Promise.resolve().then(()=>{
    if (flag){
      we_accToken.expireAt = 0
    }
    if ((!we_accToken.token) || (we_accToken.expireAt < Date.now() )){
      var options = {
      hostname: 'api.weixin.qq.com',
      path: `/cgi-bin/token?grant_type=client_credential&appid=${we_id}&secret=${we_secret}`,
      method: 'GET'}
      return app.httpsParser(options).then((httpsData:string)=>{
          return JSON.parse(httpsData)
        }).then((result)=>{
          if (result.errcode) {
            throw (new Error(result.errmsg))
          } else if (result.access_token) {
            we_accToken.token= result.access_token
            we_accToken.expireAt = (Date.now()) + result.expires_in * 1000
            return we_accToken.token
          } else {
            throw (new Error('No valid token'))
          }
        })
    } else{
      return we_accToken.token
    }
  })
}

// Express helpers
app.logger = function(req, res, next){
  debug(`No. ${incomingCnt++} @${(new Date()).toLocaleString('zh-CN',{hour12:false})}`)
  next()
}
app.wechatAuth = function (req, res, next) {
  let q = req.query
  if (q.signature && q.timestamp && q.nonce) {
    let arr = [we_token, q.timestamp, q.nonce].sort().join('')
    let sha1 = crypto.createHash('sha1')
    let result = sha1.update(arr).digest('hex')
    if (result === q.signature) {
      debug('Wechat validation passed.')
      if(q.echostr){
       res.end(q.echostr)
      }else{
        next()
      }
    } else {
      debug('Wechat validation failed.')
      res.end()
    }
  } else {
    debug('Non-wechat server request.')
    res.end()
  }
}

app.errHandler = function errHandler (err, req, res, next) {
  debug(err.message)
  res.sendStatus(500)
}




// Service logic
interface xmlReq {
    MsgType?: string[],
    MsgId?:number[],
    Event?:string[]
  }

interface xmlObj {
  xml:xmlReq
}

app.dispatcher = function (req, res, next){
  xmlParser(req.body).then((xmlObj:xmlObj)=>{
    let xmlReq:xmlReq = xmlObj.xml || {}
    if (!xmlReq.MsgId || msgIds.has(xmlReq.MsgId[0])){
      debug("Duplicated message or no valid message id, just bypass. ID:", xmlReq.MsgId||"N/A")
      next()
     } else {
        debug(msgIds)
        let id = xmlReq.MsgId[0]
        msgIds.add(id)
        setTimeout(function() {
          msgIds.delete(id)
        }, 200000)
     }
    debug(`Handling type {${xmlReq.MsgType}}`)
    switch((xmlReq.MsgType || [])[0]){
      case "text" : 
      app.handleText(xmlReq, res, next)
      debug("text branch hit.")
      break
      case "event" :
      app.hanldeEvent(xmlReq, res, next)
      debug("Event branch hit.")
      break
      default : res.end()
      }
  }).catch((e)=>{
    debug("Exception in dispatcher \n",e)
    res.end()
  })
}

app.sendMsg = function(userId,msg){
  return app.getToken().then((token)=>{
      let msgOut = {
      touser:userId,
      msgtype:"text",
      text:{content:msg}
    }
      let postData = JSON.stringify(msgOut)
      let options = {
      hostname: 'api.weixin.qq.com',
      path: `/cgi-bin/message/custom/send?access_token=${token}`,
      method: 'POST',
      headers:{'Content-Length': Buffer.byteLength(postData)},
      postData: postData
    }
      return app.httpsParser(options)
    }).then((result)=>{
      // handle possible wechat message error
      return
    }).catch((e)=>{
      debug("Exception when sending message. Error:",e)
    })
}

app.handleText  = function(xml, res, next){
  // app.sendMsg(xml, xml.Content[0])
  debug("User msg:", xml.Content[0])
  next()
}

app.handleEvent = function(xml, res, next){
  switch((xml.Event || [])[0]){
    // case "subscribe"
  }
  next()
} 

app.echoFac = function(options){
  let _expects = options.expects
  let _fns = options.fns
  let _inputs = []
  let _current = 0
  return function(input){
    if (input.match(_expects(_current).regExp)){
      _inputs.unshift(input)
      if(_current < (_fns.length - 1)){
      _fns[_current++]()
      }else{
        this.handler = def
        _fns[_current++]()
      }
    } else{
      app.sendMsg(this.attr.wechat, _expects(_current).info)
    }
  }
}

app.finalizer = function (req,res,next) {
  res.end()
}



// APP integration

app.use(helmet())
app.use(app.logger)
app.use(app.wechatAuth)
app.use(parser.text({type:'*/*'}))
app.use(app.dispatcher)
app.use(app.finalizer)
app.use(app.errHandler)


// app.listen(80)
app.listen(3001)


