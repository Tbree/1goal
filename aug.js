//augmentation inspired in Javascript the good part
var path = require('path')
var aug = {};

aug.typeof = function (obj) {
	return Object.prototype.toString.apply(obj);
}

aug.do = function(){
	// Add method to all objects. By which a method can be easily added to the prototype of the object and be shared with all the related objects.
	Object.prototype.method = function(name,func){
		if((aug.typeof(name) === "[object String]") && (aug.typeof(func) === "[object Function]"))
		{
			this.__proto__[name] = func;
			return name + "has been added to " + this.toString();
		}
		else
		{
			return "Wrong parameter(s)!";
		}
	};

	console.log("Object#method has been added.");

	// Add reload method that will force Node to reload certain module.
	global.reload = function(name){
		var fPath = path.resolve(name);
		console.log(fPath);
		delete require.cache[fPath];
		return require(fPath);
	};
	console.log("Global#reload has been added.");

	// Add reload method that will force Node to reload certain module.
	global.pf = console.log;
	console.log("Global#pf has been added.");

	// Add objectt.values

	Object.prototype.values = function (obj){
		var obj_keys = Object.keys(obj);
		var obj_values = [ ];
		for (obj_key of obj_keys ) {
			obj_values.push(obj[obj_key]);
		};
		return obj_values;
	}

	// Add typeof function that return the type string of the object
	Object.prototype.typeof = function(){
		return aug.typeof(this).replace(/\[object ([\w]+)\]/,"$1");
	};
	console.log("Object#typeof has been added.");


	// Add keys that return all keys of this object
	Object.prototype.keys = function(){
		return Object.keys(this)[0] !== undefined ? Object.keys(this) : undefined;
	};
	console.log("Object#keysof has been added.");

	Object.prototype.values = function(){
		return Object.values(this)[0] !== undefined ? Object.values(this): undefined;
	}



	/* Temp function for Euro gambling;
	global.gambling = function(team1,team2){
		switch (Math.floor(Math.random()*3)){
		case 0:
			return "Tie";
		case 1:
			return team1 || "Team 1";
		case 2:
			return team2 || "Team 2";
		default:
			return "Something mystery happend!"
		}
	}
	*/
	global.dh = function(err,data){
		if (err) {
			console.log(err);}
		else if (data.rows){
			console.log(data.rows);}
		else {
			console.log(data);};
	};
	console.log("generic data handler added.")

	// make function in object stringifiable and parsable
	global.JSONfn = {
		stringify: function(obj) {
   		 return JSON.stringify(obj,function(key, value){
           		 return (typeof value === 'function' ) ? value.toString() : value;
        			});
  			},
		parse: function(str) {
   		 return JSON.parse(str,function(key, value){
       			 if(typeof value != 'string') return value;
        		return ( value.substring(0,8) == 'function') ? eval('('+value+')') : value;
    			});
		}
	};

	return "DONE";
};

aug.do()
module.exports = aug;
